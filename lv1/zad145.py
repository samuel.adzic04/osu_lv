ham_counter = 0
ham_number_of_words = 0
spam_counter = 0
spam_number_of_words = 0
spam_exclamation_mark = 0

fhand = open('SMSSpamCollection.txt')
for line in fhand:
    if line.startswith('ham'):
        ham_counter += 1
        ham_number_of_words += len(line.split()[1:])  
    elif line.startswith('spam'):
        spam_counter += 1
        spam_number_of_words += len(line.split()[1:])
        if line.strip().endswith('!'):  
            spam_exclamation_mark += 1

print("Prosječan broj riječi u ham porukama:", ham_number_of_words/ham_counter)
print("Prosječan broj riječi u spam porukama:", spam_number_of_words/spam_counter)
print("Broj spam poruka koje završavaju uskličnikom:", spam_exclamation_mark)
